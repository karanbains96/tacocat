var createError = require('http-errors');
var express = require('express');
var http = require('http').Server(express);
var io = require('socket.io')(http);
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var favicon = require('serve-favicon');

var bodyParser = require('body-parser');
var session = require('express-session');

var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var configDB = require('./db.js');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var passportRouter = require('./routes/passport');
var appRouter = require('./routes/appRouter');
var chatRouter = require('./routes/chatRouter');

var app = express();

//connect to mongodb
const dbconnect = mongoose.connect(configDB.url);
dbconnect.then((db) => {
  console.log("Connected to mongoDB server");
}, (err) => { console.log(err); });

//prepare server
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js')); // redirect bootstrap JS
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist')); // redirect JS jQuery
app.use('/js', express.static(__dirname + '/node_modules/socket.io-client/dist')); // redirect socket.io
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css')); // redirect CSS bootstrap

//configure passport middleware
app.use(session({ secret: 'tacocattacocattacocat' })); // session secret
app.use(passport.initialize());
app.use(passport.session());

// Configure passport-local to use account model for authentication
var User = require('./models/user');
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//favicon
app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon', 'favicon.ico')));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/', passportRouter);
app.use('/dashboard', appRouter);
app.use('/chat', chatRouter);

//connect to socket.io
io.on('connection', function (socket) {
  console.log('a user connected');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
