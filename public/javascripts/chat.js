$(function () {
    let socket = io();
    let username = $('#chat-username').text();
    let toId, toUsername = "";

    //send user details
    socket.emit('update list', username);

    //receive users list
    socket.on('users list', (userlist) => {
        $('#online-no').text(userlist.length - 1); //not include yourself in count
        $("#online-users").empty();
        const index = userlist.findIndex((user) => user.username == username);
        userlist.splice(index, 1); //remove yourself from list
        $.each(userlist, (i, user) => {
            $('#online-users').append($(`<button id="${user.id}" class="online-user list-group-item list-group-item-action">`).text(user.username));
        });
    });
    
    //send message
    $('form').submit(() => {
        let msg = $('#msg').val();
        socket.emit('msg sent', msg, username, toId, toUsername);
        $('#msg').val('');
        return false;
    });

    //receive message
    socket.on('msg received', (msg) => {
        $('#msg-list').append($('<li class="list-group-item">').text(msg));
    });

    //acknowledgement message
    socket.on('receipt', (msg) => {
        $('#msg-list').append($('<li class="list-group-item">').text(msg));
    });

    //select user to message
    $(document).on('click', '.online-user', (e) => {
        toId = e.target.id;
        toUsername = e.target.textContent;
        $('#chat-helper').hide();
        $('#chat-header').html(toUsername + '<hr>');
        $('#msg').removeAttr("disabled");
        $('.online-user').removeClass("active");
        $(e.target).addClass("active");
    });
});