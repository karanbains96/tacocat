var express = require('express');
var router = express.Router();
var passport = require('passport');
var Account = require('../models/user');
var User = require('../models/user');

// =========================================================================
// passport session setup ==================================================
// =========================================================================
// required for persistent login sessions
// passport needs ability to serialize and unserialize users out of session

// used to serialize the user for the session
passport.serializeUser(function (user, done) {
    done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function (id, done) {
    Account.findById(id, function (err, user) {
        done(err, user);
    });
});

router.get('/register', function (req, res) {
    res.render('register', {});
});

router.post('/register', function (req, res, next) {
    console.log('registering user');
    Account.register(new Account({username: req.body.username}), req.body.password, function (err) {
        if (err) {
            console.log('error while user register!', err);
            return next(err);
        }

        console.log('user registered!');
        res.redirect('/');
    });
});

router.post('/login', function (req, res, next) {

    passport.authenticate('local', function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            res.render('home', {user: req.user, message: "Incorrect username or password!"});
        }
        else {
            User.findOne({username: req.body.username}).exec(function (err2, result2) {
                console.log(result2);
                if (err2) res.render('home', {user: req.user, message: "Incorrect username or password!"});
                else {
                    req.logIn(user, function (err) {
                        if (err) {
                            return next(err);
                        }
                        res.redirect('/dashboard');
                    });
                }
            });
        }
    })(req, res, next);
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});

module.exports = router;