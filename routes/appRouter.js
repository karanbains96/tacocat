var express = require('express');
var router = express.Router();

router.get('/', isLoggedIn, (req, res, next) => {
    res.render('dashboard', { title: 'Dashboard', username: req.user.username });
});

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

module.exports = router;